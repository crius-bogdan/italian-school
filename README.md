# Project for Italian School

## Used technologies

* PUG
* SCSS
* JavaScript (native)
* SVG Sprite
* Gulp

[Link](https://crius-bogdan.gitlab.io/italian-school/ "Link to view the site")
