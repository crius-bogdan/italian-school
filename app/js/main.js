(function(doc){
    doc.addEventListener('DOMContentLoaded', function (e) {
        // ==> Служебные переменные
        const dataset = 'dataset',
              clas = 'classList',
              activeClass = 'active',
              parentNode = 'parentNode',
              optionsPresentationSlider = {
                slidesPerView: 'auto',
                navigation: {
                    prevEl: '.h-pres-prev',
                    nextEl: '.h-pres-next',
                }
              },
              optionsPresentationPageSliders = {
                slidesPerView: 'auto',
                navigation: {
                    prevEl: '.pres-all-prev',
                    nextEl: '.pres-all-next',
                }
              };

        function getElem(name, wrap) {
            if (wrap) {
                return wrap.querySelector(name);
            } else {
                return doc.querySelector(name);
            }
        }
        function getElems(name, wrap) {
            if (wrap) {
                return wrap.querySelectorAll(name);
            } else {
                return doc.querySelectorAll(name);
            }
        }
        function atr(elem, attrubute) {
            return elem.getAttribute(attrubute);
        }
        function match(element, className) {
            return element[clas].contains(className);
        }
        function eventWorker(mainArray, subArray, id) {
            for (let i = 0; i < mainArray.length; i++) {
                if (id == mainArray[i][dataset].id) {
                    mainArray[i][clas].add(activeClass);
                    subArray[i][clas].add(activeClass);
                } else {
                    mainArray[i][clas].remove(activeClass);
                    subArray[i][clas].remove(activeClass);
                }
            }
        }
        function pluses() {
            for (let x = 0; x < inputsQuest.length; x++) {
                if (inputsQuest[x].checked) {
                    answer += +inputsQuest[x].value;
                }
            }
            console.log(answer, 'ansver');
            testForm.submit();
        }
        const popupMenu = getElem('.m-pop'),
              popMainLinks = getElems('.pop-lnk'),
              popHideContent = getElems('.pop-cont'),
              overlay = getElem('.overlay'),
              presentationsPageItems = getElems('.pres-all'),
              blogTabTitle = getElems('.bl-titl'),
              blogTabCont = getElems('.bl-cont'),
              lessonBtnOpener = doc.querySelectorAll('.s-opener'),
              lessonContentOpenes = getElems('.s-op'),
              numberOfqest = getElem('.num-t'),
              inputsQuest = getElems('.t-inp'),
              testForm = getElem('.t-from'),
              testItems = getElems('.t-itm'),
              comingModal = getElem('.coming'),
              mainMenu = getElem('.menu'),
              burgerBtn = getElem('.mob-menu'),
              scrollTestResultBtn = getElem('.sc-btn');
        
        let answer = null;

        function smoothScroll(target) {
            if (target.hash !== '') { 
                const hash = target.hash.replace("#", "")
                const link = doc.getElementsByName(hash);
                //Find the where you want to scroll
                const position = link[0].getBoundingClientRect().y 
                let top = 0

                let smooth = setInterval(() => {
                    let leftover = position - top
                    if (top === position) {
                        clearInterval(smooth)
                    }

                    else if(position > top && leftover < 10) {
                        top += leftover
                        window.scrollTo(0, top)
                    }

                    else if(position > (top - 10)) {
                        top += 10
                        window.scrollTo(0, top)
                    }

                }, 6)//6 milliseconds is the faster chrome runs setInterval
            }
        }

        // scrollTestResultBtn.addEventListener('click', smoothScroll);
        popupMenu.addEventListener('mousemove', function(e) {
            let target = e.target;

            // while()

            if (match(target, 'pop-lnk')) {
                let targetId = target[dataset].id;
                
                eventWorker(popMainLinks, popHideContent, targetId);
                
                return;
            }
        });
        doc.addEventListener('click', function(e) {
            let target = e.target;
            while (target != this) {
                
                if (match(target, 'pop-op') || match(target, 'show')) {
                    if (window.innerWidth < 1200) return;
                    e.preventDefault();
                    popupMenu[clas].toggle(activeClass);
                    // popupMenu.style.top = `${window.pageYOffset + 99}px`
                    overlay[clas].toggle('show');
                    return;
                }

                if (match(target, 'mob-menu') || match(target, 'modMenu')) {
                    if (match(burgerBtn, activeClass)) {

                        burgerBtn[clas].remove(activeClass);
                        mainMenu[clas].remove('open');
                        overlay[clas].remove('modMenu');
                        if (window.innerWidth < 766) {
                            doc.querySelector('.com')[clas].remove('mob-m')
                        }
                        return;
                    }

                    burgerBtn[clas].add(activeClass);
                    mainMenu[clas].add('open');
                    overlay[clas].add('modMenu');
                    if (window.innerWidth < 766) {
                        doc.querySelector('.com')[clas].add('mob-m')
                    }
                    return; 
                }

                if (match(target, 'com') || match(target, 'showMod')) {
                    e.preventDefault();
                    comingModal[clas].toggle(activeClass);
                    overlay[clas].toggle('showMod');
                    return;
                }

                if(match(target, 'bl-titl')) {
                    if(match(target, activeClass)) {
                        return;
                    }

                    let targId = target[dataset].id;
                    eventWorker(blogTabCont, blogTabTitle, targId);
                    
                    return;
                }

                if (match(target, 's-opener')) {
                    let targId = target[dataset].id;
                    if(!match(target, activeClass)) {
                        eventWorker(lessonContentOpenes, lessonBtnOpener, targId);
                    } else {
                        for (let i = 0; i < lessonContentOpenes.length; i++) {
                            if (targId == lessonContentOpenes[i][dataset].id) {
                                lessonContentOpenes[i][clas].remove(activeClass);
                                lessonBtnOpener[i][clas].remove(activeClass);
                            } 
                            continue;
                        }
                    }
                }
                
                if (match(target, 't-nxt')) {
                    e.preventDefault();
                    for (let i = 0; i < testItems.length; i++) {
                        let idx = 0;
                        if (match(testItems[i], 'active')) {
                            // console.log(currentQuest);
                            idx = idx + +testItems[i][dataset].id;
                            
                            
                            if(idx < 10) {
                                numberOfqest.textContent = idx + 1;
                                testItems[i][clas].remove(activeClass);
                                testItems[idx][clas].add(activeClass);
                            } else {
                                // console.log(inputsQuest, 'checkbox-value');
                                pluses();
                            }
                            
                            return;
                        }
                    }
                }

                if (match(target, 'sc-btn')) {
                    e.preventDefault();
                    smoothScroll(target);
                }

                if (match(target, 'ink-go')) {
                    window.location = atr(target, 'data-href');
                }
                target = target[parentNode];
            }
            
        });
        // doc.addEventListener('scroll', function(e){
        //     // if (window.pageYOffset > )
        //     popupMenu.style.top = `${window.pageYOffset + 99}px`
        // });
        
        
        
        
        
        
        
        svg4everybody({});
        const homePresentation = new Swiper('.h-pres', optionsPresentationSlider);
        const homeMedia = new Swiper('.h-med', {
            
            spaceBetween: 21,
            // loop: true,
            // effect: 'fade',
            navigation: {
                prevEl: '.h-med-prev',
                nextEl: '.h-med-next',
            },
            breakpoints: {
                320: {
                    slidesPerView: 1,
                },
                768: {
                    slidesPerView: 2,
                },
                1026: {
                    slidesPerView: 'auto',
                },
            },
        });
        if (presentationsPageItems) {
            for (let i = 0; i < presentationsPageItems.length; i++) {
                new Swiper(presentationsPageItems[i], optionsPresentationPageSliders);
            }
        }
    });
})(document);